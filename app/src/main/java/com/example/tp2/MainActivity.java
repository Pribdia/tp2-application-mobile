package com.example.tp2;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    WineDbHelper wineDbHelper = null;
    SimpleCursorAdapter cursorAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        wineDbHelper = new WineDbHelper(this);
        if (wineDbHelper.fetchAllWines().getCount() < 1) {
            wineDbHelper.populate();
        }
        Cursor test = wineDbHelper.fetchAllWines();

        cursorAdapter =
                new SimpleCursorAdapter(this,
                        android.R.layout.simple_list_item_2, wineDbHelper.fetchAllWines(), new String[] {
                        wineDbHelper.COLUMN_NAME,
                        wineDbHelper.COLUMN_WINE_REGION },
                        new int[] { android.R.id.text1, android.R.id.text2 }, 0);
        // Associate the adapter with the list view
        ListView lv = (ListView) findViewById(R.id.list_wine);
        registerForContextMenu(lv);
        lv.setAdapter(cursorAdapter);

        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                //Wine wineSelected = wineDbHelper.getWineById(id);

                String selectID="_id="+id;
                SQLiteDatabase db=wineDbHelper.getReadableDatabase();
                Cursor wineSelect=db.query(wineDbHelper.TABLE_NAME,null, selectID,null,null,null,null,null);
                Wine wineSelected=wineDbHelper.cursorToWine(wineSelect);

                Toast.makeText(MainActivity.this, "LE VIN !!! -> "+ wineSelected.getTitle(), Toast.LENGTH_SHORT).show();

                /*Intent wineView = new Intent(MainActivity.this, WineActivity.class);
                wineView.putExtra("wineSelected",wineSelected);
                startActivity(wineView);*/
            }
        });

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Wine wineAdd = new Wine("Titre défaut","Region défaut","Localisation défaut", "Climate défaut", "Surface planté défaut");
                wineDbHelper.addWine(wineAdd);
                cursorAdapter.swapCursor(wineDbHelper.fetchAllWines());

                Snackbar.make(view, "Un vin par defaut à était créé", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        if (v.getId() == R.id.list_wine) {
            MenuInflater inflater = getMenuInflater();
            inflater.inflate(R.menu.menu_main, menu);
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();

        if (item.getItemId() == R.id.action_delete) {
            SQLiteDatabase db = wineDbHelper.getWritableDatabase();

            String selectedId = "_id=" + info.id;
            Cursor cursor = db.query(wineDbHelper.TABLE_NAME,null,selectedId,null,null,null,null);
            wineDbHelper.deleteWine(cursor);
            cursorAdapter.swapCursor(wineDbHelper.fetchAllWines());
            return true;
        }
        return false;
    }
}
